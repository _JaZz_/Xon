const Discord = require("discord.js");

exports.run = (client, message, args, level) => {
  
  if(level < 9) {
    return message.channel.send('Sorry looks like u dont have access here :grey_question: ')
  }
  
  const config = require('../config')
  if(!args || args.size < 1) return message.reply("Must provide a command name to reload.");
  const commandName = args[0];
  // Check if the command exists and is valid
  const embed1 = new Discord.RichEmbed()
  .setAuthor('Error')
  .setTitle('Command not found')
  .setColor(config.bot_color);
  if(!client.commands.has(commandName)) {
    return message.reply(embed1);
  }
  // the path is relative to the *current folder*, so just ./filename.js
  delete require.cache[require.resolve(`./${commandName}.js`)];
  // We also need to delete and reload the command from the client.commands Enmap
  client.commands.delete(commandName);
  const props = require(`./${commandName}.js`);
  client.commands.set(commandName, props);
  const embed = new Discord.RichEmbed()
  .setAuthor('Successful')
  .setTitle(`${commandName} was reloaded`)
  .setColor(config.bot_color);
 message.channel.send(embed)
};