const fs = require("fs");

exports.run = (client, message) => {

fs.readdir("./commands/", (err, files) => {
  if (err) return console.error(err);
  files.forEach(file => {
    if (!file.endsWith(".js")) return;
    let props = require(`./commands/${file}`);
    let commandName = file.split(".")[0];
    message.channel.send(`${commandName}`);
    
    client.commands.set(commandName, props);
  });
});
}