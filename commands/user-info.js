const Discord = require("discord.js");
const config = require("../config")

                              
exports.run = (client, message, args) => {
  
          const UserInfo = new Discord.RichEmbed()
            .setAuthor(message.author.username, message.author.avatarURL || message.author.defaultAvatarURL()) 
            .setTitle('UserInfo')
            .setColor(config.bot_color) 
            .setThumbnail(message.author.avatarURL)

            .addField('Bot', message.author.bot, true)
 
            .addField('Discrim', message.author.discriminator, true)
            .addField('DMChannel', message.author.dmChannel, true) 
            .addField('ID', message.author.id, true) 
            .addField('Last Message', message.author.lastMessage, true) 
            .addField('Last Message ID', message.author.lastMessageID, true) 
            .addField('Tag', message.author.tag, true) //The Discord "tag" for this user || Ex:- Sai Chinna#6718
            .addField('Username', message.author.username, true) //The username of the user || Ex:- Sai Chinna
            .addField('Created At', message.author.createdAt, false) //The time the user was created || .createdTimestamp - The         
            .setTimestamp() //The timestamp of this embed

        message.channel.send(UserInfo);
}