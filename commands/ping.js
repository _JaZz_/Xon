const Discord = require("discord.js");

exports.run = (client, message, args) => {
  
  const config = require('../config')
 const embed = new Discord.RichEmbed()
.setAuthor('🏓  Pong')
.setColor(config.bot_color)
.setTitle(`**${Math.round(message.client.ping)}ms**`)
    message.channel.send(embed);
}