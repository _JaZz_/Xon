const dialogflow = require('dialogflow');
const config = require('../config.js');

const LANGUAGE_CODE = 'en-US' 

exports.run = async (client, message, args, level) => {
  
class DialogFlow {
	constructor (projectId) {
		this.projectId = projectId

		let privateKey = (config.NODE_ENV=="production") ? JSON.parse(config.Dialogflow) : config.Dialogflow
		let clientEmail = config.client_email
		let config = {
			credentials: {
				private_key: privateKey,
				client_email: clientEmail
			}
		}
	
		this.sessionClient = new dialogflow.SessionsClient(config)
    console.log('logged in dialogflow')
	}

 
  
	async sendTextMessageToDialogFlow(textMessage, sessionId) {
		// Define session path
		const sessionPath = this.sessionClient.sessionPath(this.projectId, sessionId);
		// The text query request.
		const request = {
			session: sessionPath,
			queryInput: {
				text: {
					text: textMessage,
					languageCode: LANGUAGE_CODE
				}
			}
		}
		try {
			let responses = await this.sessionClient.detectIntent(request)			
			console.log('DialogFlow.sendTextMessageToDialogFlow: Detected intent');
			return responses
      message.channel.send(responses)
		}
    
		catch(err) {
			console.error('DialogFlow.sendTextMessageToDialogFlow ERROR:', err);
			throw err
		}
    
	}
}
}