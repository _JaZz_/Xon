const discord = require("discord.js")
const config = require("../config.js")
exports.run = async (client, msg, args, level) => {
  const message = msg
  
  if(level < 9) {
    return message.channel.send('Sorry looks like u dont have access here :grey_question: ')
  }
  
  const embed = new discord.RichEmbed()
  .setAuthor(client.user.username, client.user.avatarURL || client.user.defaultAvatarURL)
  .setTitle('Rebooting the bot')
  .setColor(config.bot_color);
 await msg.channel.send(embed)
  console.log(client.user.username + ' is Rebooting')
 process.exit(0)
  client.login(config.token)
  msg.channel.send('rebooted')
  }