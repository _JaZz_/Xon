exports.run = async (client, message, args, level, name) => {
  // eslint-disable-line no-unused-vars
  const discord = require("discord.js");
  const config = require('../config.js');
  const embed = new discord.RichEmbed()
  .setAuthor('Perm-Level')
  .setTitle(`You are __**${name}**__ with permlevel of __**${level}**__`)
  .setColor(config.bot_color)
	message.reply(embed);
};

exports.help = {
	name: 'permlevel',
	category: 'Miscelaneous',
	description: 'Returns your permission level',
	usage: 'permlevel'
};