const exec = require('child_process').exec;
const discord = require('discord.js');
const config = require('../config.js')
exports.run = async(client, message, args, level) => {
  if(level < 9) {
    return message.channel.send('Sorry looks like u dont have access here :grey_question: ')
  }  
  exec(`${args.join(' ')}`, (error, stdout) => {
      const response = (error || stdout);
      const output = new discord.RichEmbed()
      .setTitle('Output')
      .addField(`Ran: ${args.join(" ")}\n${response}`, {code: "asciidoc", split: "\n"})//.catch(console.error)
      .setColor(config.bot_color)
      message.channel.send(output);
    });
};

exports.help = {
  name: "exec",
  category: "System",
  description: "Executes command prompt code",
  usage: "exec [...code]"
};