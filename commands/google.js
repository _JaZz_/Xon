const google = require('google');
const config = require('../config')
const Discord = require(`discord.js`);
exports.run = (client, message) => {
    let args = message.content.split(/[ ]+/);
    let suffix = args.slice(1).join(' ');
  console.log(message.author.username + ' ' + 'Googled' + ' ' + suffix)
    if (!suffix) {
        message.channel.send({
            embed: {
                color: config.bot_color,
                description: `:warning: **${message.author.username}**, You should write something for me. \n!!google \`'you want to search'\``,
                footer: {
                    text: 'Google Search',
                }
            }
        });
    }
    google.resultsPerPage = 25;
    google(suffix, function (err, res) {
        if (err) message.channel.send({
            embed: {
                color: config.bot_color,
                description: `\n:warning: **${message.author.username}**, write google what you want to call`,
                footer: {
                    text: 'Google Search',
                }
            }
        });
      for (var i = 0; i < res.links.length; ++i) {
            var link = res.links[i];
            if (!link.href) {
                res.next;
            } else {
                let embed = new Discord.RichEmbed()
                    .setColor(`#ffffff`)
                    .setAuthor(`Result "${suffix}"`, `https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/2000px-Google_%22G%22_Logo.svg.png`)
                    .setDescription(`\n**Link**: [${link.title}](${link.href})\n**Information**:\n${link.description}`)
                    .setTimestamp()
                    .setFooter('Google Search', message.author.displayAvatarURL);
                return message.channel.send({
                    embed: embed
                });
            } return message.react("✅");
        }
    });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: 0
};

exports.help = {
  name: 'google',
  description: "It gives you the link to google best of what you're typing (ie searches google too)",
  usage: 'google search'
};