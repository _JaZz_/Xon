const Discord = require("discord.js")
const config = require('../config')
const { version } = require("discord.js");
const moment = require("moment");
const m = require("moment-duration-format");
let os = require('os')
let cpuStat = require("cpu-stat")
const bot_data = require('../package.json')

exports.run = (bot, message, args) => {
    let cpuLol;
    cpuStat.usagePercent(function(err, percent, seconds) {
        if (err) {
            return console.log(err);
        }
        const duration = moment.duration(bot.uptime).format(" D [days], H [hrs], m [mins], s [secs]");
        const embedStats = new Discord.RichEmbed()
            .setAuthor('Bot-Stats', 'https://ntldstats.com/ntldstats_logo_v3.png')
            .setColor(config.bot_color)
            .addField("• Mem Usage", `${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} / ${(os.totalmem() / 1024 / 1024).toFixed(2)} MB`, true)
            .addField("• Uptime ", `${duration}`, true)
            .addField("• Users", `${bot.users.size.toLocaleString()}`, true)
            .addField("• Servers", `${bot.guilds.size.toLocaleString()}`, true)
            .addField("• Bot-version ", bot_data.version , true)
            .addField("• Discord.js", `v${version}`, true)
            .addField("• Node", `${process.version}`, true)
            .addField("• API Latency", `${Math.round(bot.ping)}ms`, true)  
            .addField("• Developers", ' |_JaZz_|#6770\nMayor Chano', true)
            .addField("• CPU", `\`\`\`md\n${os.cpus().map(i => `${i.model}`)[0]}\`\`\``)
            .addField("• CPU usage", `\`${percent.toFixed(2)}%\``, true)
            .addField("• Arch", `\`${os.arch()}\``, true)
            .addField("• Platform", `\`\`${os.platform()}\`\``, true)
 
        message.channel.send(embedStats)
    });
};


exports.help = {
    name: "stats"
};