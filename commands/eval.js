var Discord = require('discord.js');
exports.run = async (client, message, args, level) => {
	var time = Date.now();
	var os = require('os')
  const code = args.join(' ');
  if(level < 9) {
    return message.channel.send('Sorry looks like u dont have access here :grey_question: ')
  }
 	if (!code) {
		return message.reply('You need to give me some code...');

  }

  	try {
		const evaled = eval(code);

		if (evaled.length > 30000) {
			message.channel.send(`${evaled}\n\nTime taken: ${Date.now() - time}ms`, { code: 'xl', split: true }).catch(console.error);
		} else {
			var evalEmbed = new Discord.RichEmbed()
				.setAuthor(client.user.username, client.user.avatarURL || client.user.defaultAvatarURL)
				.setTitle('Eval Output')
				.setColor('#00a0ff')
				.setDescription(`\`\`\`xl\n${evaled}\`\`\``)
				.setFooter(`Time taken: ${Date.now() - time}ms`);
			message.channel.send({ embed: evalEmbed }).catch(console.error);		}
	} 
  catch (err) {
		if (err.message.length < 150) {
			var errorEmbed = new Discord.RichEmbed()
				.setTitle('ERROR')
        .setDescription(err)
				.setColor('#00a0ff')
				.setAuthor(client.user.username, client.user.avatarURL || client.user.defaultAvatarURL)
				.setFooter(`Time taken: ${Date.now() - time}ms`);
			message.channel.send({ embed: errorEmbed }).catch(console.error);
		} else {
			message.channel.send(`\`ERROR (Check console for error stack)\` \`\`\`xl\n${await client.clean(client, err.message)}\n\nTime taken: ${Date.now() - time}ms\n\`\`\``, { split: true }).catch(console.error);
		}
		console.log(err);
	}


exports.help = {
	name: 'eval',
	category: 'System',
	description: 'Evaluates arbitrary javascript. With great power comes great responsibility',
	usage: 'eval [code]' 
}
}