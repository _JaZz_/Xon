ItzXon is a Multipurpose D-Bot
=================

Xon is licensed under `MIT`. Updates to your code will instantly deploy and update live.

**Xon** is the friendly discord bot where you'll able to chat with AI, Google, and Much-More. Its is the app of your dreams. Xon lets you instantly Play Music, remix, edit, and host an app, bot or site, and you can invite collaborators or helpers to simultaneously edit code with you.

Find out more [Xon](Xon.com).

Your Project
------------

On the front-end,
- edit `public/client.js`, `public/style.css` and `views/index.html`
- drag in `assets`, like images or music, to add them to your project

On the back-end,
- your app starts at `server.js`
- add frameworks and packages in `package.json`
- safely store app secrets in `.env` (nobody can see this but you and people you invite)


Copyright 2018 Xon
-------------------
