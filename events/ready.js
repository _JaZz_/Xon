const config = require('../config.js');


module.exports = (client, guild,) => {
  
  console.log(`Ready to serve as ${client.user.username} in ${client.channels.size} channels on ${client.guilds.size} servers, for a total of ${client.users.size} users.`);
  client.user.setStatus(config.Status)
    client.user.setPresence({
        game: {
            name: config.Gamename,
            type: config.Gametype,
            url:  config.Url
        }
    });
};